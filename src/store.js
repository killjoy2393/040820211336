import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    comments: [
      {
        name: 'Самуил',
        text: 'Привет, Верунь! ниче себе ты крутая. фотка класс!!!!',
        date: '13 октября 2011'
      },
      {
        name: 'Лилия Семёновна',
        text: 'Вероника, здравствуйте! Есть такой вопрос: Особый вид куниц жизненно стабилизирует кинетический момент, это и есть всемирно известный центр огранки алмазов и торговли бриллиантами?',
        date: '14 октября 2011'        
      },
      {
        name: 'Лилия Семёновна',
        text: 'Вероника, здравствуйте! Есть такой вопрос: Особый вид куниц жизненно стабилизирует кинетический момент?',
        date: '14 октября 2011'        
      }
    ]

  },

  getters: {
    COMMENTS: state => {
      return state.comments.slice(-3)
    },

    ALL_COMMENTS: state => {
      return state.comments
    }
  },

  mutations: {
    SET_COMMENT: (state, comment) => {
      state.comments.push(comment)
    }
  },
  actions: {

  }
})
